#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
DATA PROCESSING PART
"""

import os
import re
import nltk
import string
import operator
from nltk.corpus import stopwords
from bs4 import BeautifulSoup as bs
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem.lancaster import LancasterStemmer


path = '/Users/ludong/Desktop/UNSW/COMP9900/NSWSC'
NSWSC = os.listdir(path)

# STOP WORDS
stop_words = set(stopwords.words('english'))

'''
to do the stem and lemma part
'''
st = LancasterStemmer()
lem = WordNetLemmatizer()


'''
This function is trying to get the title and the article content separately for each file
'''
def get_article_and_title_content(file):

    f = open(file, 'r')
    soup = bs(f, 'html.parser')
    text = soup.get_text()

    title = soup.title.string
    # print(title)
    title_content = title.split()
    len_of_title = len(title_content)
    # print(title_content)

    contents = text.split()
    if '[Help]' in contents:
        index_of_help = contents.index('[Help]')
    else:
        index_of_help = contents.index('Help')
    reversed_contents = contents[::-1]
    index_of_AustLII = reversed_contents.index('AustLII:')

    new_contents = contents[index_of_help + len_of_title + 1:(len(contents) - index_of_AustLII - 1)]

    # print(new_contents)

    article = ' '.join(new_contents)

    return article, title_content

'''
Get the words from each title and process them
'''
def get_title_words(title_content):

    title_words = []
    for word in title_content:
        if word.lower() not in stop_words and \
                        len(word) > 1 and \
                        word.lower() not in string.punctuation and \
                word.lower().isalpha():
            tmp = lem.lemmatize(word.lower())
            w = st.stem(tmp)
            title_words.append(w)

    return title_words


'''
Get the words from each article content and process them
'''
def get_real_words(article):

    sens = nltk.sent_tokenize(article)

    all_sentences_in_words = [nltk.word_tokenize(sentence) for sentence in sens]

    words = []
    for sen_words in all_sentences_in_words:
        for word in sen_words:
            if word.isupper() and len(word) > 1:
                words.append(word)
            elif word.lower() not in stop_words and\
                    len(word) > 1 and\
                     word.lower() not in string.punctuation and\
                    word.lower().isalpha():
                words.append(word.lower())

    tags = nltk.pos_tag(words)

    real_words = []

    for i in range(len(words)):
        tag = tags[i]
        # print(words[i], tag[1])
        # print(type(tag[1]))
        if re.match('V', tag[1]):
            # print(words[i], tag[1])
            # print('***')
            # lemmatization
            tmp = lem.lemmatize(words[i], pos='v')
        else:
            # lemmatization
            tmp = lem.lemmatize(words[i])
        # after lemmatization, stemming
        st_word = st.stem(tmp)
        real_words.append(st_word)

    return real_words


'''
This function is used for tfidf to get the counts of each word
'''
def get_count_of_word(words):
    dic = {}
    for i in words:
        if i not in dic:
            dic[i] = 1
        else:
            dic[i] += 1
    new_dic = sorted(dic.items(), key=operator.itemgetter(1), reverse = True)

    return new_dic

'''
article_index
    a dictionary: key is the index of each article, value is the name of it
    {0:'NSWSC_1993_1', '1':NSWSC_1994_1,...}
    
article_words
    a dictionary: key is the index of each article, value is a list that stores all the words after processing of this article

article_counts
    a dictionary: key is the index of each article, value is a dictionary stores all the words and their counts in this article
    
title_index
    a dictionary: key is the index of each article, value is a list stores all the words after processing of this article's title
'''
article_index = {}   
article_words = {}
article_counts = {}
title_index = {}


'''
DATA PROCESS
'''
index = 0
for title in NSWSC:

    # those files are either 404 or empty
    if title in ['NSWSC_2009_812.html','NSWSC_2009_888.html',
                 'NSWSC_2012_423.html', 'NSWSC_2012_1675.html','.DS_Store',
                 'NSWSC_2015_2136.html','NSWSC_2011_64.html','NSWSC_2014_1981.html']:
        pass
    else:
        file = '{}/{}'.format(path, title)
        name = title[6:len(title)-5]
#        print(index)
#        print(file)
        article, title_content = get_article_and_title_content(file)
        article_index[index] = name
        real_words = get_real_words(article)
        article_words[index] = real_words
        counts = get_count_of_word(real_words)
        title_words = get_title_words(title_content)
        title_index[index] = title_words
        # print(real_words)
        # t = {}
        # for word in real_words:
        #     if word not in inverted_index:
        #         inverted_index[word] = [index]
        #     else:
        #         inverted_index[word].append(index)
        article_counts[index] = counts
        # inverted_index = get_inverted_index(real_words, title)
        # break
        index += 1
   

'''
Write those new data we got into JSON files for next step's using
'''  
import json

article_index_json = json.dumps(article_index, indent=4)

output_article_index = open('/Users/ludong/Desktop/UNSW/COMP9900/output_article_index.json', 'w')
output_article_index.write(article_index_json)
output_article_index.close()

article_words_json = json.dumps(article_words, indent=4)

output_article_words = open('/Users/ludong/Desktop/UNSW/COMP9900/output_article_words.json', 'w')
output_article_words.write(article_words_json)
output_article_words.close()


article_counts_json = json.dumps(article_counts, indent=4)

output_article_counts = open('/Users/ludong/Desktop/UNSW/COMP9900/output_article_counts.json', 'w')
output_article_counts.write(article_counts_json)
output_article_counts.close()

title_index_json = json.dumps(title_index, indent=4)

output_title_index = open('/Users/ludong/Desktop/UNSW/COMP9900/output_title_index.json', 'w')
output_title_index.write(title_index_json)
output_title_index.close()